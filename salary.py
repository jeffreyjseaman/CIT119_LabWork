base_hours = 40 # Base hours per week
ot_hours_calc = 1.5 # OT Hours

# Get the hours worked and the hourly pay rate
hours = float(input('Enter the number of hours worked this week:?  '))
payrate = float(input('Enter the hourly wage:?  '))

# Conditional Statement
if hours > base_hours:
    # Calcuate the gross pay with OT
    ot_hours = hours - base_hours

    # Calculate the amount of OT pay
    ot_pay = ot_hours * payrate * ot_hours_calc

    # Calculate the gross pay
    grosspay = base_hours * payrate + ot_pay
else:
    grosspay = hours * payrate

    # Display the gross pay
print('The gross pay is $', format(grosspay, ',.2f'), sep='')

