#Determine if customer qualifies for a loan

minsalary = 20000.00 #The min salary annually
minyears = 2 # The min years at job
creditscore = 400 # The min credit score

#Get the customers annual salary
salary = float(input('Enter your annual salary:?  '))

# Get the number of years at your job
jobyears = int(input('Enter the number of years employed:?  '))

##Hard Code a value
##Web Service call to Equifax( They sent us a value )
getcreditscore = 300

#Determine if the customer qualifies
if salary >= minsalary:  #Step 1 process
    if jobyears >= minyears: #Step 2 process
        if getcreditscore >= creditscore: #Step 3 process
            print('You qualify for the loan.')
        elif getcreditscore < creditscore:
            print('You failed to meet the credit score')
        else:
            print('You must have been employed',\
                  'for at least',minyears,\
                  'years to qualify.')
else:
    print('You must earn at least $',\
          format(minsalary, '.2f'),\
          ' per year to qualify.', sep='')

