package application;

//Step1: Add Libraries
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

//Step 2: Add on
public class Switch extends Application {

	Stage window; //Step 5: Call window instead of Stage. Easier to remember.
	Scene scene1, scene2; //How many scenes you are going to create. You would initialize here.
	//Step 3 Copy from the other program
	public static void main(String[] args) {
		launch(args);  

	}
	
	//Step 4: Start
	public void start(Stage primaryStage) throws Exception {
		//Step6 declare window = primary stage
		window = primaryStage;
		
		Label label1 = new Label("Welcome to the first scene"); //Step 7
		Button button1 = new Button("Go to scene 2"); //Step 8
		button1.setOnAction(e -> window.setScene(scene2)); //Step 9. Using Lamda Expression
		
		//Layout1 = children are laid out in vertical column
		//different layouts that we didn't discuss. Page 552 discusses 7 different ones. We are going to use VBox
		VBox layout1 = new VBox(20); //Step 10
		layout1.getChildren().addAll(label1, button1); //Step 11: In swing we would do an add object individually. Easier now
		scene1 = new Scene(layout1, 400, 400); //Step 12: What layout we are using, and the size of the scene.
		
		//Step 13: Creating Button for the second scene. Copy info over
		Button button2 = new Button("This is the wrong scene, go back to scene 1"); //Step 13
		button2.setOnAction(e -> window.setScene(scene1)); //Step 14. 
		
		//Layout2
		StackPane layout2 = new StackPane(); //Step 15
		layout2.getChildren().add(button2); //Step 16
		scene2 = new Scene(layout2, 600,600); //Step 17 Same as above
		
		window.setScene(scene1); //Step 18
		window.setTitle("JavaFX Week 4"); //Step 19
		window.show(); //Step 20

	}

}

