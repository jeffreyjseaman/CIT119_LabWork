from tkinter import *

root = Tk()

def info():
    print('works')

def Edit():
    print('Edit works')

mymenu = Menu(root)
root.config(menu=mymenu)

filemenu = Menu(mymenu)
mymenu.add_cascade(label='File', menu=filemenu)

filemenu.add_command(label='New notebook', command=info)
filemenu.add_command(label='New Desktop', command=info)
filemenu.add_separator()
filemenu.add_command(label='New Mobile', command=info)

editmenu = Menu(mymenu)


##Have students add Edit Menu,
##Create new Function
##Create submenu

editmenu.add_command(label='Undo Typing',command=Edit)
editmenu.add_command(label='Cut',command=Edit)
editmenu.add_separator()
editmenu.add_command(label='Copy',command=Edit)

###All new on 26-29 & new Function
mymenu.add_cascade(label="Edit", menu=editmenu) ##Orginal

root.mainloop() ##Original