from tkinter import *

root = Tk()

mymenu = Menu(root)
root.config(menu=mymenu)

filemenu = Menu(mymenu)
mymenu.add_cascade(label='File',menu=filemenu)

editmenu = Menu(mymenu)
mymenu.add_cascade(label='Edit',menu=editmenu)

root.mainloop()